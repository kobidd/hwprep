import { AuthorService } from './../author.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  authors$:Observable<any>
  //authorName:string
  //authorId:string
  //authors:object[]= [{id:1,name:'Lewis Carrol'},{id:2,name:'Leo Tolstoy'},{id:3,name:'Thomas Mann'},{id:4,name:'Amos Oz'}]
  newAuthor:string
  constructor(private router:Router,
              private authorservice:AuthorService) { }

  ngOnInit() {
    //this.authorName = this.activatedroute.snapshot.params['name'];
    //this.authorId = this.activatedroute.snapshot.params['id'];
    this.authors$ = this.authorservice.getAuthors()
    }
    onSubmit(){
      this.authorservice.addAuthor(this.newAuthor)
    }
  }


