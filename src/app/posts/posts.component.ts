import { AuthService } from './../auth.service';
import { Post } from './../interfaces/post';
import { PostsService } from './../posts.service';
import { forkJoin, Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  postDb$:Observable<any>;
  posts$:Post[];
  users$:User[];
  check:Boolean = false
  PageTitle:string
  title:string
  username:string
  body:string
  userId
  constructor(private postsservice:PostsService,
              private router:Router,
              private authservice:AuthService) { }

 deletePost(id:string){
    this.postsservice.deletePost(this.userId,id);
    }
  
  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid; 
        this.postDb$ = this.postsservice.getPosts(this.userId);
        console.log(user)
      }
    )

    /*forkJoin(this.postsservice.getPostsData(),this.postsservice.getUsersData()).subscribe(
      ([data1,data2])=>{
        this.posts$ = data1
        this.users$ = data2
        console.log(data1)
        console.log(data2)
      }
    )*/
  /*this.postsservice.getPostsData()
    .subscribe(
      data=>{
        console.log(data)
        this.posts$ = data
      }
    )
   this.postsservice.getUsersData()
   .subscribe(data=>{
     console.log(data)
     this.users$=data
   })*/
  }

  onSubmit(){
this.router.navigate(['postform']);

    /*for(let i=0;i<this.posts$.length;i++){
    for(let j=0;j<this.users$.length;j++){
      if(this.posts$[i].userId==this.users$[j].id){
        this.title = this.posts$[i].title
        this.body = this.posts$[i].body
        this.username = this.users$[j].name
        this.postsservice.addPosts(this.title,this.username,this.body)
        console.log("in")
      }
    }
  }
    this.check = true;
    this.PageTitle = 'The Data loaded to DB'
  }*/
 /*private saveparams(post$:Post[],users$:User[]) {
  for(let i = 0;i<this.posts$.length ;i++){
    for(let j = 0; j<this.users$.length ;j++){
      if(this.posts$[i].userId == this.users$[j].uid){
        this.posts$[i].username == this.users$[j].username
      }
    }
  }
  }*/
 
  }
}
