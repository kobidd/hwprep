import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
authorName:string
autorId:string
  constructor(private activatedroute:ActivatedRoute,
              private router:Router) { }

  ngOnInit() {
    this.authorName = this.activatedroute.snapshot.params['name'];
    this.autorId = this.activatedroute.snapshot.params['id'];
  }
  onSubmit(){
    this.router.navigate(['/authors',this.authorName,this.autorId]);
  }

}
