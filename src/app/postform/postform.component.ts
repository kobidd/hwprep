import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {
title:string
body:string
author:string
id:string
editMode:boolean = false;
buttonText:string = "Add Post"
userId:string
  constructor(private postsservice:PostsService,
              private router:Router, 
              private route:ActivatedRoute,
              private authservice:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.authservice.user.subscribe(
      user=>{
        this.userId = user.uid
        if(this.id){
          this.editMode = true
          this.buttonText = "Updae Post"
          this.postsservice.getPost(this.userId,this.id).subscribe(
            post=>{
              this.title = post.data().title;
              this.author = post.data().author;
              this.body = post.data().body;
            }
          )
        }
      }
    )
    console.log(this.id)
  }
  onSubmit(){
    if(this.editMode){
      this.postsservice.updatePost(this.userId,this.id,this.title,this.author,this.body);
    }
    else{
    this.postsservice.addPost(this.userId,this.title,this.author,this.body);
  }
    this.router.navigate(['posts'])
  }

}
