import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  authors:any= [{id:1,name:'Lewis Carrol'},{id:2,name:'Leo Tolstoy'},{id:3,name:'Thomas Mann'},{id:4,name:'Amos Oz'}]
  constructor() { }
  getAuthors(){
    const authorsObservable = new Observable(
      authors=>{
        setInterval(
          ()=> authors.next(this.authors),4000
        )
      }
    )
    return authorsObservable
  }
  addAuthor(author:string){
    this.authors.push({name:author})
  }
}
