import { Router } from '@angular/router';
import { UserAuth } from './interfaces/user-auth';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
user:Observable<UserAuth|null>
  constructor(public afAuth:AngularFireAuth,
              public router:Router) { 
    this.user = this.afAuth.authState;
  }

  signUp(email:string,password:string){
    this.afAuth
    .auth
    .createUserWithEmailAndPassword(email,password)
    .then(user =>{ 
      this.router.navigate(['posts'])})
  }

  logOut(){
    this.afAuth.auth.signOut().then(res=>console.log('Log Out',res))
    this.router.navigate(['login'])
  }

  logIn(email:string,password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(user =>{ 
    this.router.navigate(['posts'])})
  }
}
