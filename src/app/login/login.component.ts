import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
email:string
password:string
  constructor(private authservice:AuthService,
              private router:Router) { }

  ngOnInit() {
  }
  onSubmit(){
    this.authservice.logIn(this.email,this.password)
  }

}
