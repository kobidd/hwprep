import { User } from './interfaces/user';
import { Post } from './interfaces/post';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';



@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private PostURL = "https://jsonplaceholder.typicode.com/posts/";
  private UserURL = "https://jsonplaceholder.typicode.com/users/";

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection:AngularFirestoreCollection;

  constructor(private http:HttpClient,
              private db:AngularFirestore) { }
getPostsData(){
  return this.http.get<Post[]>(`${this.PostURL}`)
}
getUsersData(){
return this.http.get<User[]>(`${this.UserURL}`)
}

/*addPosts(title:string,author:string,body:string){
  const post = {title:title,author:author,body:body}
  this.db.collection('posts').add(post);
}*/
getPosts(userId:string):Observable<any>{
  //return this.db.collection('posts').valueChanges({idField:'id'});
  this.postCollection = this.db.collection(`users/${userId}/posts`);
  return this.postCollection.snapshotChanges().pipe(
    map(collection=>collection.map(
      document=>{
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data
      }
    ))
  )
}
addPost(userId:string,title:string,author:string,body:string){
  const post = {title:title,author:author,body:body}
  //this.db.collection('posts').add(post);
  this.userCollection.doc(userId).collection('posts').add(post);
}
deletePost(userId:string,id:string){
  this.db.doc(`users/${userId}/posts/${id}`).delete();
}
getPost(userId:string,id:string){
  return this.db.doc(`users/${userId}/posts/${id}`).get();
}
updatePost(userId:string,id:string,title:string,author:string,body:string){
this.db.doc(`users/${userId}/posts/${id}`).update({
  title:title,
  author:author,
  body:body}
)
}
}
