export interface UserAuth {
    uid:string,
    email?: string | null,
    photoUrl?: string,
    displayName?:string

}
