// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig:{
    apiKey: "AIzaSyCjN7TZdkPFFDNdEEQ2P70egTGzRm1VkZY",
    authDomain: "hwprep-f6ca7.firebaseapp.com",
    databaseURL: "https://hwprep-f6ca7.firebaseio.com",
    projectId: "hwprep-f6ca7",
    storageBucket: "hwprep-f6ca7.appspot.com",
    messagingSenderId: "91758763410",
    appId: "1:91758763410:web:6497da196b1cf9f79b354b",
    measurementId: "G-V9KMLHFZWY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
